package me.flyray.admin.biz;

import me.flyray.admin.entity.Config;
import me.flyray.admin.mapper.ConfigMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import me.flyray.common.biz.BaseBiz;

/** 
* @author: bolei
* @date：2018年4月8日 下午2:20:01 
* @description：类说明
*/

@Service
@Transactional(rollbackFor = Exception.class)
public class ConfigBiz extends BaseBiz<ConfigMapper, Config> {

}
