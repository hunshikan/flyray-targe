package me.flyray.admin.biz;

import me.flyray.admin.entity.GateLog;
import me.flyray.admin.mapper.GateLogMapper;
import me.flyray.common.biz.BaseBiz;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-07-01 14:36
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GateLogBiz extends BaseBiz<GateLogMapper, GateLog> {

    @Override
    public void insert(GateLog entity) {
        mapper.insert(entity);
    }

    @Override
    public int insertSelective(GateLog entity) {
    	return mapper.insertSelective(entity);
    }
}
