package me.flyray.admin;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import me.flyray.auth.client.EnableAceAuthClient;
import com.spring4all.swagger.EnableSwagger2Doc;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * ${DESCRIPTION}
 *
 * @author wanghaobin
 * @create 2017-05-25 12:44
 */
@EnableDiscoveryClient
@EnableCircuitBreaker
@SpringBootApplication
@EnableFeignClients({"me.flyray.auth.client.feign","me.flyray.admin.feignclient"})
@EnableScheduling
@ComponentScan({"me.flyray.admin","me.flyray.cache","me.flyray.auth.client"})
@EnableAceAuthClient
@EnableTransactionManagement
@MapperScan("me.flyray.admin.mapper")
@EnableSwagger2Doc
public class FlyrayAdminBootstrap {
    public static void main(String[] args) {
        new SpringApplicationBuilder(FlyrayAdminBootstrap.class).run(args);
    }
}
