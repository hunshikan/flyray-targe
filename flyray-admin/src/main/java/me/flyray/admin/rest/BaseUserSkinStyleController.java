package me.flyray.admin.rest;

import me.flyray.admin.biz.BaseUserSkinStyleBiz;
import me.flyray.admin.entity.BaseUserSkinStyle;
import me.flyray.admin.vo.BaseUserSkinStyleRequest;
import me.flyray.common.rest.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.Map;

@Controller
@RequestMapping("baseUserSkinStyle")
public class BaseUserSkinStyleController extends BaseController<BaseUserSkinStyleBiz, BaseUserSkinStyle> {
	
	/**
	 * 查询用户皮肤
	 * @author centerroot
	 * @time 创建时间:2018年8月31日上午10:42:03
	 * @param baseUserSkinStyleRequest
	 * @return
	 */
	@RequestMapping(value = "/queryObj", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryObj(@RequestBody @Valid BaseUserSkinStyleRequest baseUserSkinStyleRequest){
        return baseBiz.queryObj(baseUserSkinStyleRequest);
	}
	/**
	 * 添加或更新用户皮肤
	 * @author centerroot
	 * @time 创建时间:2018年8月30日下午3:44:50
	 * @param baseUserSkinStyleRequest
	 * @return
	 */
	@RequestMapping(value = "/addOrUpdate", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> addOrUpdate(@RequestBody @Valid BaseUserSkinStyleRequest baseUserSkinStyleRequest){
		return baseBiz.addOrUpdate(baseUserSkinStyleRequest);
	}
}