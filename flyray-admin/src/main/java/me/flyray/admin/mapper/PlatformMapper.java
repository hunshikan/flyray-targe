package me.flyray.admin.mapper;

import me.flyray.admin.entity.Platform;
import tk.mybatis.mapper.common.Mapper;

public interface PlatformMapper extends Mapper<Platform> {
}