package me.flyray.admin.mapper;

import me.flyray.admin.entity.Config;
import tk.mybatis.mapper.common.Mapper;

public interface ConfigMapper extends Mapper<Config> {
}