package me.flyray.admin.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;


/**
 * 
 * 
 * @author admin
 * @email ${email}
 * @date 2019-03-05 19:50:36
 */
@Data
@Table(name = "base_application")
public class BaseApplication implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //
    @Id
    private Integer id;
	
	    //应用编号
    @Column(name = "app_id")
    private String appId;
	
	    //应用名称
    @Column(name = "app_name")
    private String appName;
	
	    //应用地址
    @Column(name = "app_url")
    private String appUrl;
	
	    //应用图标
    @Column(name = "app_icon")
    private String appIcon;
	
	    //应用简介
    @Column(name = "app_profile")
    private String appProfile;
	
	    //是否新窗口中打开
    @Column(name = "is_new_tab")
    private Integer isNewTab;
	


}
