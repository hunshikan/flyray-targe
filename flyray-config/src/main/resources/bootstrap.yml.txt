server:
  #本地测试环境
  port: 4001
  #测试环境端口
  #port: 8087
spring:
  application:
    name: flyray-config
  #  profiles:
  #    active: dev
  cloud:
    config:
      server:
        git:
          uri: https://gitee.com/flyray/flyray.git
          username: 1222                                       # git仓库的账号
          password: 1222                                               # git仓库的密码
          default-label: dev


#-----------开发环境配置-------------#
---
spring:
  #  profiles: dev
  cloud:
    consul:
      host: 127.0.0.1
      port: 8500
      discovery:
        enabled: true
        service-name: ${spring.application.name}
        health-check-interval: 10s
        tags: urlprefix-/${spring.application.name}
        # 可以设置自定义健康监测接口
        health-check-path: /actuator/health
        # 自定义脚本监测配置false
        # register: false
        register: true

endpoints:
  health:
    sensitive: false

#-----------测试环境配置-------------#
---
spring:
  profiles: test
  cloud:
    consul:
      host: 127.0.0.1
      port: 8500
      discovery:
        enabled: true
        service-name: ${spring.application.name}
        health-check-interval: 10s
        tags: urlprefix-/${spring.application.name}
        # 可以设置自定义健康监测接口
        health-check-path: /actuator/health
        # 自定义脚本监测配置false
        # register: false
        register: true
