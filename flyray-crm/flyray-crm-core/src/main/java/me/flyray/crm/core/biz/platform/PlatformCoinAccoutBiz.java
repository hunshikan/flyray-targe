package me.flyray.crm.core.biz.platform;

import me.flyray.crm.core.mapper.PlatformCoinAccoutMapper;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;
import me.flyray.crm.core.entity.PlatformCoinAccout;

/**
 * 
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-12-21 15:00:16
 */
@Service
public class PlatformCoinAccoutBiz extends BaseBiz<PlatformCoinAccoutMapper,PlatformCoinAccout> {
}