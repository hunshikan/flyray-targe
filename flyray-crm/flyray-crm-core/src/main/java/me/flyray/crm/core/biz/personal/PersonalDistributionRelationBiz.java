package me.flyray.crm.core.biz.personal;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.entity.PersonalDistributionRelation;
import me.flyray.crm.core.mapper.PersonalDistributionRelationMapper;
import me.flyray.crm.facade.request.DistributionRelationQueryRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;

/**
 * 个人分销关系
 * @email ${email}
 * @date 2018-07-16 10:15:49
 * http://blog.csdn.net/cctcc/article/details/53992215
 */
@Service
@Slf4j
public class PersonalDistributionRelationBiz extends BaseBiz<PersonalDistributionRelationMapper, PersonalDistributionRelation> {

	@Autowired
	private PersonalDistributionRelationMapper personalDistributionRelationMapper;

	/**
	 * 列表
	 */
	public List<PersonalDistributionRelation> distributionRelations(DistributionRelationQueryRequest param) {
		log.info("查询分享关系，请求参数。。。{}"+param);
		return mapper.queryByPersonalId(param.getCustomerId());
	}

	public List<PersonalDistributionRelation> queryByPersonalId(String personalId){
		return personalDistributionRelationMapper.queryByPersonalId(personalId);
	}

	/**
	 * 查询直接推荐人
	 * @param personalId
	 * @return
	 */
	public PersonalDistributionRelation queryReferrerByPersonalId(String personalId){
		return personalDistributionRelationMapper.queryReferrerByPersonalId(personalId);
	}



}