package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-12-21 15:00:16
 */
@Table(name = "platform_coin_transaction_to")
public class PlatformCoinTransactionTo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //平台编号
    @Column(name = "platform_id")
    private Long platformId;
	
	    //入账地址
    @Column(name = "to_address")
    private String toAddress;
	
	    //交易金额
    @Column(name = "amount")
    private BigDecimal amount;
	
	    //交易hash
    @Column(name = "hash")
    private Long hash;
	
	    //1：奖励 2：转账 3：手续费
    @Id
    private String transactionType;
	
	    //商户号
    @Column(name = "merchant_id")
    private String merchantId;
	

	/**
	 * 设置：平台编号
	 */
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：平台编号
	 */
	public Long getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：入账地址
	 */
	public void setToAddress(String toAddress) {
		this.toAddress = toAddress;
	}
	/**
	 * 获取：入账地址
	 */
	public String getToAddress() {
		return toAddress;
	}
	/**
	 * 设置：交易金额
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * 获取：交易金额
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * 设置：交易hash
	 */
	public void setHash(Long hash) {
		this.hash = hash;
	}
	/**
	 * 获取：交易hash
	 */
	public Long getHash() {
		return hash;
	}
	/**
	 * 设置：1：奖励 2：转账 3：手续费
	 */
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	/**
	 * 获取：1：奖励 2：转账 3：手续费
	 */
	public String getTransactionType() {
		return transactionType;
	}
	/**
	 * 设置：商户号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户号
	 */
	public String getMerchantId() {
		return merchantId;
	}
}
