package me.flyray.crm.core.schedule;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import me.flyray.common.enums.AccountType;
import me.flyray.common.enums.CustomerType;
import me.flyray.common.enums.PointsTradeType;
import me.flyray.crm.core.biz.customer.CustomerBaseBiz;
import me.flyray.crm.core.biz.customer.CustomerIntoAccountBiz;
import me.flyray.crm.core.biz.personal.PersonalAccountBiz;
import me.flyray.crm.core.entity.CustomerBase;
import me.flyray.crm.core.entity.PersonalAccount;
import me.flyray.crm.core.mapper.CustomerBaseMapper;
import me.flyray.crm.facade.request.IntoAccountRequest;
import me.flyray.crm.facade.request.PersonalAccountRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * Description:
 *
 * @param:
 * @return:
 * @date: 2018/11/27/027 15:11
 */
@Slf4j
@Component
public class PointAccountTask {

    @Value("${current.platform.id}")
    private String platformId = "13608825924956160";

    @Autowired
    private PersonalAccountBiz personalAccountBiz;

    @Autowired
    private CustomerIntoAccountBiz customerIntoAccountBiz;

    @Autowired
    private CustomerBaseMapper customerBaseMapper;


    /**
     * <p>1. 将积分账户复制到历史积分账户</p>
     * <p>2. 记录转账流水</p>
     * <p>3. 如果存在历史账户，扣减积分账户，扣减历史账户</p>
     * <p>4. 记录扣减流水</p>
     *
     * @param:
     * @return:
     * @date: 2018/11/27/027 15:23
     */
    @Scheduled(cron = "0 0 0 1 1 ?")
    public void PointAccountSettleTask() {
        log.info("PointAccountSettleTask start...");
        //获取前海的所有积分账户
        List<PersonalAccount> pointAccountList = queryPointAccounts(platformId);
        //复制到历年账户
        pointAccountList.parallelStream().filter(pointAccount -> {
            return pointAccount.getAccountType().equals(AccountType.POINT.getCode()) ||
                    pointAccount.getAccountType().equals(AccountType.HIS_POINT.getCode());
        }).forEach(pointAccount -> {
            //如果是积分账户
            handle(pointAccount);
        });


    }

    /**
     *
     * @param pointAccount
     */
    private void handle(PersonalAccount pointAccount) {
        if(pointAccount.getAccountType().equals(AccountType.POINT.getCode())){
            try {
                CustomerBase customerBase = new CustomerBase();
                customerBase.setPlatformId(pointAccount.getPlatformId());
                customerBase.setPersonalId(pointAccount.getPersonalId());
                customerBase = customerBaseMapper.selectOne(customerBase);
                if(customerBase == null){
                    return;
                }

                IntoAccountRequest intoAccountRequest = new IntoAccountRequest();
                intoAccountRequest.setPersonalId(pointAccount.getPersonalId());
                intoAccountRequest.setPlatformId(pointAccount.getPlatformId());
                intoAccountRequest.setCustomerId(customerBase.getCustomerId());
                intoAccountRequest.setAccountType(AccountType.HIS_POINT.getCode());
                intoAccountRequest.setIntoAccAmt(String.valueOf(pointAccount.getAccountBalance()));
                intoAccountRequest.setCustomerType(CustomerType.CUST_PERSONAL.getCode());
                intoAccountRequest.setTradeType(PointsTradeType.EXPIRE_POINTS_EXCHANGE.getCode());
                customerIntoAccountBiz.intoAccount(intoAccountRequest);
                log.info("Point Account handle ,account ={}", JSON.toJSONString(pointAccount));
            }catch (Exception e){

            }
        }
        else{
            //TODO 积分账户

            /*OutAccountRequest outAccountRequest = new OutAccountRequest();
            outAccountRequest.setAccountType(AccountType.POINT.getCode());
            outAccountRequest.setPersonalId(pointAccount.getPersonalId());
            outAccountRequest.setCustomerId(pointAccount.getAccountId());
            outAccountRequest.setCustomerType(CustomerType.CUST_PERSONAL.getCode());
            outAccountRequest.setTradeType(TradeType.SETTLEMENT.getCode());
            customerOutAccountBiz.outAccount(outAccountRequest);
            log.info("Point Account handle ,account ={}", JSON.toJSONString(pointAccount));
            */
        }

    }

    //获取积分账户 TODO 分页接口
    private List<PersonalAccount> queryPointAccounts(String platformId) {
        PersonalAccountRequest personalAccountRequest = new PersonalAccountRequest();
        personalAccountRequest.setPlatformId(platformId);
        Map<String, Object> map = personalAccountBiz.queryList(personalAccountRequest);
        if (map != null) {
            return (List<PersonalAccount>) map.get("personalAccountList");
        }
        return null;
    }
}
