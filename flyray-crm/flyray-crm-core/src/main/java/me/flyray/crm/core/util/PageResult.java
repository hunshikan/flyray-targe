package me.flyray.crm.core.util;

import lombok.Data;

import java.io.Serializable;
@Data
public class PageResult implements Serializable {
    private Integer rowCount;
    private Integer pageCount;
    private Integer currentPage;
    private Integer pageSize;
}
