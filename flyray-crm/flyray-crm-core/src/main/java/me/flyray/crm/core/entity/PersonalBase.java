package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import me.flyray.common.entity.BaseEntity;
import org.springframework.format.annotation.DateTimeFormat;


/**
 * 个人客户基础信息
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */

@Data
@Table(name = "personal_base")
public class PersonalBase extends BaseEntity implements Serializable {
	private static final long serialVersionUID = 1L;
	
	//序号
    @Column(name = "id")
    private Integer id;
    
	    //个人客户编号

	@Id
    @Column(name = "personal_id")
    private String personalId;
	
	    //平台编号
    @Column(name = "platform_id")
    private String platformId;
	
	    //用户编号
    @Column(name = "customer_id")
    private String customerId;
	
	    //第三方会员编号
    @Column(name = "third_no")
    private String thirdNo;
	
	    //手机号
	@Column(name = "phone")
	private String phone;
	
	    //用户名称
    @Column(name = "real_name")
    private String realName;
	
	    //身份证号
    @Column(name = "id_card")
    private String idCard;
	
	    //用户昵称
    @Column(name = "nickname")
    private String nickname;
	
	    //性别 1：男 2：女
    @Column(name = "sex")
    private String sex;
	
	    //生日
    @Column(name = "birthday")
    private String birthday;
	

	    //居住地
    @Column(name = "address")
    private String address;
	
	    //身份证正面
    @Column(name = "id_positive")
    private String idPositive;
	
	    //身份证反面
    @Column(name = "id_negative")
    private String idNegative;

	/**
	 * 业务自定义状态：认证状态  00：未认证  01：无需认证  02：认证成功  03：认证失败
	 * * 设置：认证状态  00：初始态
	 */
    @Column(name = "authentication_status")
    private String authenticationStatus;
	
	    //账户状态 00：正常，01：冻结
    @Column(name = "status")
    private String status;
	
	    //创建时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "create_time")
    private Date createTime;
	
	    //更新时间
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "update_time")
    private Date updateTime;
	
    // 归属人id（存后台管理系统登录人员id）指所属客户经理id
    @Column(name = "owner_id")
    private String ownerId;

	// 归属人名称（存后台管理系统登录人员名称）所属哪个客户经理
	@Column(name = "owner_name")
	private String ownerName;

    // 用户头像
    @Column(name = "avatar")
    private String avatar;
    
	    //备注
	@Column(name = "remark")
	private String remark;
	
	    //个人客户类型  00：注册用户  01：运营添加用户
	@Column(name = "personal_type")
	private String personalType;
    
	@Transient
    private String parentId;
	
	@Transient
    private String fxLevel;
    @Transient
    private String parentLevel;

}
