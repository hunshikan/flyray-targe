package me.flyray.crm.core.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * 
 * 
 * @author centerroot
 * @email ${email}
 * @date 2018-12-21 15:00:16
 */
@Table(name = "platform_coin_accout_journal")
public class PlatformCoinAccoutJournal implements Serializable {
	private static final long serialVersionUID = 1L;
	
	    //交易流水id
    @Id
    private Long journalId;
	
	    //交易所属平台
    @Column(name = "platform_id")
    private Long platformId;
	
	    //交易地址
    @Column(name = "address")
    private String address;
	
	    //交易类型：奖励、转账、手续费、
    @Column(name = "trade_type")
    private String tradeType;
	
	    //来往标志  1：来账   2：往账
    @Column(name = "in_out_flag")
    private String inOutFlag;
	
	    //交易金额
    @Column(name = "amount")
    private BigDecimal amount;
	
	    //客户类型 平台客户 普通客户
    @Column(name = "customer_type")
    private String customerType;
	
	    //商户号
    @Column(name = "merchant_id")
    private String merchantId;
	

	/**
	 * 设置：交易流水id
	 */
	public void setJournalId(Long journalId) {
		this.journalId = journalId;
	}
	/**
	 * 获取：交易流水id
	 */
	public Long getJournalId() {
		return journalId;
	}
	/**
	 * 设置：交易所属平台
	 */
	public void setPlatformId(Long platformId) {
		this.platformId = platformId;
	}
	/**
	 * 获取：交易所属平台
	 */
	public Long getPlatformId() {
		return platformId;
	}
	/**
	 * 设置：交易地址
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * 获取：交易地址
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * 设置：交易类型：奖励、转账、手续费、
	 */
	public void setTradeType(String tradeType) {
		this.tradeType = tradeType;
	}
	/**
	 * 获取：交易类型：奖励、转账、手续费、
	 */
	public String getTradeType() {
		return tradeType;
	}
	/**
	 * 设置：来往标志  1：来账   2：往账
	 */
	public void setInOutFlag(String inOutFlag) {
		this.inOutFlag = inOutFlag;
	}
	/**
	 * 获取：来往标志  1：来账   2：往账
	 */
	public String getInOutFlag() {
		return inOutFlag;
	}
	/**
	 * 设置：交易金额
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	/**
	 * 获取：交易金额
	 */
	public BigDecimal getAmount() {
		return amount;
	}
	/**
	 * 设置：客户类型 平台客户 普通客户
	 */
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	/**
	 * 获取：客户类型 平台客户 普通客户
	 */
	public String getCustomerType() {
		return customerType;
	}
	/**
	 * 设置：商户号
	 */
	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}
	/**
	 * 获取：商户号
	 */
	public String getMerchantId() {
		return merchantId;
	}
}
