package me.flyray.crm.core.biz.platform;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.flyray.common.msg.BizResponseCode;
import me.flyray.crm.core.entity.PlatformCallbackUrl;
import me.flyray.crm.core.entity.PlatformSafetyConfig;
import me.flyray.crm.core.mapper.PlatformCallbackUrlMapper;
import me.flyray.crm.core.mapper.PlatformSafetyConfigMapper;
import me.flyray.crm.facade.request.CallBackUrlRequest;
import me.flyray.crm.facade.request.PlatformCallbackUrlRequest;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.flyray.common.biz.BaseBiz;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.common.util.EntityUtils;

import lombok.extern.slf4j.Slf4j;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 平台回调地址配置
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:48
 */
@Slf4j
@Service
public class PlatformCallbackUrlBiz extends BaseBiz<PlatformCallbackUrlMapper, PlatformCallbackUrl> {
	
	@Autowired
	private PlatformSafetyConfigMapper platformSafetyConfigMapper;
	
	public TableResultResponse<PlatformCallbackUrl> callbackUrlList(CallBackUrlRequest bean){
		 Example example = new Example(PlatformCallbackUrl.class);
		 Criteria criteria = example.createCriteria();
		 criteria.andEqualTo("platformId",bean.getPlatformId());
		 example.setOrderByClause("create_time desc");
		 Page<PlatformCallbackUrl> result = PageHelper.startPage(bean.getPage(), bean.getLimit());
		 List<PlatformCallbackUrl> list = mapper.selectByExample(example);
		 return new TableResultResponse<PlatformCallbackUrl>(result.getTotal(), list);
	}
	
	
	public Map<String, Object> addCallbackUrl(CallBackUrlRequest bean){
		PlatformCallbackUrl config = new PlatformCallbackUrl();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 log.info("【添加回调地址】 请求参数。。。"+config);
			 config.setCreateTime(new Date());
			 config.setUpdateTime(new Date());
			 if (mapper.insert(config) > 0) {
				respMap.put("code", BizResponseCode.OK.getCode());
				respMap.put("message", BizResponseCode.OK.getMessage());
				log.info("【添加回调地址】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			 e.printStackTrace();
			log.info("【添加回调地址】  报错：" + e.getMessage());
			respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			log.info("【添加回调地址】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
		respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
		log.info("【添加回调地址】   响应参数：{}", respMap);
		return respMap;
	}
	
	
	public Map<String, Object> updateCallbackUrl(CallBackUrlRequest bean){
		PlatformCallbackUrl config = new PlatformCallbackUrl();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 config.setUpdateTime(new Date());
			 if (mapper.updateByPrimaryKeySelective(config) > 0) {
				respMap.put("code", BizResponseCode.OK.getCode());
				respMap.put("message", BizResponseCode.OK.getMessage());
				log.info("【修改回调地址】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【修改回调地址】 报错：" + e.getMessage());
			respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			log.info("【修改回调地址】  响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
		 respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
		 log.info("【修改回调地址】  响应参数：{}", respMap);
		 return respMap;
	}
	
	
	public Map<String, Object> deleteCallbackUrl(CallBackUrlRequest bean){
		 PlatformCallbackUrl config = new PlatformCallbackUrl();
		 Map<String, Object> respMap = new HashMap<>();
		 try {
			 BeanUtils.copyProperties(config, bean);
			 if (mapper.delete(config) > 0) {
				respMap.put("code", BizResponseCode.OK.getCode());
				respMap.put("message", BizResponseCode.OK.getMessage());
				log.info("【删除回调地址】   响应参数：{}", respMap);
				return respMap;
			}
		 } catch (Exception e) {
			e.printStackTrace();
			log.info("【删除安全配置】   报错：" + e.getMessage());
			respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
			respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
			log.info("【删除回调地址】   响应参数：{}", respMap);
			return respMap;
		 }
		 respMap.put("code", BizResponseCode.SERVICE_NOT_AVALIABLE.getCode());
		 respMap.put("message", BizResponseCode.SERVICE_NOT_AVALIABLE.getMessage());
		 log.info("【删除回调地址】   响应参数：{}", respMap);
		 return respMap;
	}
	
	/**
	 * 根据平台查询回调地址
	 * @param params
	 * @return
	 */
	public Map<String, Object> queryCallbackUrlByPlatformId(PlatformCallbackUrlRequest params){
		log.info("【根据平台查询回调地址】   请求参数：{}", EntityUtils.beanToMap(params));
		Map<String, Object> respMap = new HashMap<String, Object>();
		PlatformCallbackUrl platformCallbackUrl = new PlatformCallbackUrl();
		platformCallbackUrl.setPlatformId(params.getPlatformId());
		platformCallbackUrl.setCallbackType(params.getCallbackType());
		PlatformCallbackUrl selectOneUrl = mapper.selectOne(platformCallbackUrl);
		if(null != selectOneUrl){
			String callbackUrl = selectOneUrl.getCallbackUrl();
			//查询盐值
			PlatformSafetyConfig platformSafetyConfig = new PlatformSafetyConfig();
			platformSafetyConfig.setPlatformId(params.getPlatformId());
			PlatformSafetyConfig selectOneConfig = platformSafetyConfigMapper.selectOne(platformSafetyConfig);
			if(null != selectOneConfig){
				respMap.put("signKey", selectOneConfig.getAppKey());
			}else{
				respMap.put("signKey", null);
			}
			respMap.put("callbackUrl", callbackUrl);
		}else{
			respMap.put("signKey", null);
			respMap.put("callbackUrl", null);
		}
		respMap.put("code", BizResponseCode.OK.getCode());
		respMap.put("message", BizResponseCode.OK.getMessage());
		log.info("【根据平台查询回调地址】   响应参数：{}", respMap);
		return respMap;
	}
}