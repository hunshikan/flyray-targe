package me.flyray.crm.core.biz.platform;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import me.flyray.common.enums.TradeType;
import me.flyray.common.msg.AccountType;
import me.flyray.common.msg.TableResultResponse;
import me.flyray.crm.core.biz.customer.CustomerBaseBiz;
import me.flyray.crm.core.entity.CustomerBase;
import me.flyray.crm.core.entity.MerchantAccount;
import me.flyray.crm.core.entity.MerchantAccountJournal;
import me.flyray.crm.core.mapper.CustomerBaseMapper;
import me.flyray.crm.core.mapper.MerchantAccountJournalMapper;
import me.flyray.crm.core.mapper.MerchantAccountMapper;
import me.flyray.crm.core.util.BeanCopyUtils;
import me.flyray.crm.facade.request.*;
import me.flyray.crm.facade.response.MerchantAccountOpBalanceResp;
import me.flyray.crm.facade.response.MerchantAccountOpJournalResponse;
import me.flyray.crm.facade.response.CustomerAccountQueryResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 平台扩展信息
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-07-16 10:15:49
 */
@Service
public class PlatformAccountExtendBiz {

    @Autowired
    private MerchantAccountMapper merchantAccountMapper;

    @Autowired
    private MerchantAccountJournalMapper merchantAccountJournalMapper;

    @Autowired
    private CustomerBaseMapper  customerBaseMapper;

    /**
     * Description:
     *
     * @param:
     * @return:
     * @date: 2019-01-17 15:18
     */
    public MerchantAccountOpBalanceResp accountBalance(MerchantBalanceOpRequest merchantBalanceOpRequest) {
        //获取该平台的所有账户
        MerchantAccount condition = new MerchantAccount();
        condition.setPlatformId(merchantBalanceOpRequest.getPlatformId());
        condition.setCustomerId(merchantBalanceOpRequest.getCustomerId());
        List<MerchantAccount> merchantAccountList = merchantAccountMapper.select(condition);
        //统计AC001 AC006的数据
        BigDecimal accountBalance = new BigDecimal(0.00);
        BigDecimal freezeBalance = new BigDecimal(0.00);
        BigDecimal countBalance = new BigDecimal(0.00);
        if (!CollectionUtils.isEmpty(merchantAccountList)) {
            accountBalance = accountBalance.add(new BigDecimal(merchantAccountList.stream().filter(merchantAccount -> merchantAccount.getAccountType().equals(AccountType.ACC_BALANCE.getCode())).
                    map(MerchantAccount::getAccountBalance).collect(Collectors.summingDouble(BigDecimal::doubleValue)))).setScale(2, RoundingMode.HALF_DOWN);
            freezeBalance = freezeBalance.add(new BigDecimal(merchantAccountList.stream().filter(merchantAccount -> merchantAccount.getAccountType().equals(AccountType.ACC_BALANCE.getCode())).
                    map(MerchantAccount::getFreezeBalance).collect(Collectors.summingDouble(BigDecimal::doubleValue)))).setScale(2, RoundingMode.HALF_DOWN);
            countBalance = countBalance.add(new BigDecimal(merchantAccountList.stream().filter(merchantAccount -> merchantAccount.getAccountType().equals(AccountType.ACC_TRANS.getCode())).
                    map(MerchantAccount::getAccountBalance).collect(Collectors.summingDouble(BigDecimal::doubleValue)))).setScale(2, RoundingMode.HALF_DOWN);
        }
        MerchantAccountOpBalanceResp merchantAccountOpBalanceResp = new MerchantAccountOpBalanceResp(merchantBalanceOpRequest.getPlatformId(), merchantBalanceOpRequest.getCustomerId(), "", accountBalance, freezeBalance, countBalance);
        return merchantAccountOpBalanceResp;
    }


    /**
     * Description:
     *
     * @param:
     * @return:
     * @date: 2019-01-17 19:07
     */
    public TableResultResponse<CustomerAccountQueryResponse> listAccount(MerchantAccountOpRequest merchantAccountOpRequest) {
        Example example = new Example(MerchantAccount.class);
        Example.Criteria criteria = example.createCriteria();
        if (!StringUtils.isBlank(merchantAccountOpRequest.getPlatformId())) {
            criteria.andEqualTo("platformId", merchantAccountOpRequest.getPlatformId());
        }
        if (!StringUtils.isBlank(merchantAccountOpRequest.getAccountType())) {
            criteria.andEqualTo("accountType", merchantAccountOpRequest.getAccountType());
        }
        if (!StringUtils.isBlank(merchantAccountOpRequest.getCustomerId())) {
            criteria.andEqualTo("customerId", merchantAccountOpRequest.getCustomerId());
        }
        if (!StringUtils.isBlank(merchantAccountOpRequest.getMerchantType())) {
            criteria.andEqualTo("merchantType", merchantAccountOpRequest.getMerchantType());
        }
        example.setOrderByClause("create_time desc");
        Page<MerchantAccount> result = PageHelper.startPage(merchantAccountOpRequest.getPage(), merchantAccountOpRequest.getLimit());
        List<MerchantAccount> list = merchantAccountMapper.selectByExample(example);
        List<CustomerAccountQueryResponse> customerAccountQueryResponses = new ArrayList<>();
        if (!CollectionUtils.isEmpty(list)) {
            list.stream().forEach(merchantAccount -> {
                CustomerAccountQueryResponse customerAccountQueryResponse = new CustomerAccountQueryResponse();
                BeanCopyUtils.copy(merchantAccount, MerchantAccount.class, customerAccountQueryResponse, CustomerAccountQueryResponse.class);
                //获取商户名称
                CustomerBase condition = new CustomerBase();
                condition.setCustomerId(merchantAccount.getCustomerId());
                condition = customerBaseMapper.selectOne(condition);
                customerAccountQueryResponse.setAccountName(condition.getUsername());
                customerAccountQueryResponses.add(customerAccountQueryResponse);
            });
        }
        return new TableResultResponse<>(result.getTotal(), customerAccountQueryResponses);
    }

    /**
     * Description:
     *
     * @param:
     * @return:
     * @date: 2019-01-17 20:46
     */
    public TableResultResponse<MerchantAccountOpJournalResponse> listJournal(MerchantAccountOpJournalRequest merchantAccountOpJournalRequest) {
        Example example = new Example(MerchantAccountJournal.class);
        Example.Criteria criteria = example.createCriteria();
        if (!StringUtils.isBlank(merchantAccountOpJournalRequest.getPlatformId())) {
            criteria.andEqualTo("platformId", merchantAccountOpJournalRequest.getPlatformId());
        }
        if (!StringUtils.isBlank(merchantAccountOpJournalRequest.getCustomerId())) {
            criteria.andEqualTo("customerId", merchantAccountOpJournalRequest.getCustomerId());
        }
        if (!StringUtils.isBlank(merchantAccountOpJournalRequest.getJournalId())) {
            criteria.andEqualTo("journalId", merchantAccountOpJournalRequest.getJournalId());
        }
        if (!StringUtils.isBlank(merchantAccountOpJournalRequest.getTradeType())) {
            criteria.andEqualTo("tradeType", merchantAccountOpJournalRequest.getTradeType());
        }
        if (!StringUtils.isBlank(merchantAccountOpJournalRequest.getStartDate())) {
            criteria.andGreaterThanOrEqualTo("createTime", merchantAccountOpJournalRequest.getStartDate());
        }
        if (!StringUtils.isBlank(merchantAccountOpJournalRequest.getEndDate())) {
            criteria.andLessThanOrEqualTo("createTime", merchantAccountOpJournalRequest.getEndDate());
        }
        example.setOrderByClause("create_time desc");
        Page<MerchantAccountJournal> result = PageHelper.startPage(merchantAccountOpJournalRequest.getPage(), merchantAccountOpJournalRequest.getLimit());
        List<MerchantAccountJournal> list = merchantAccountJournalMapper.selectByExample(example);
        List<MerchantAccountOpJournalResponse> merchantAccountOpJournalResponses = new ArrayList<>();
        if (!CollectionUtils.isEmpty(list)) {
            list.stream().forEach(merchantAccountJournal -> {
                MerchantAccountOpJournalResponse merchantAccountOpJournalResponse = new MerchantAccountOpJournalResponse();
                BeanCopyUtils.copy(merchantAccountJournal, MerchantAccountJournal.class, merchantAccountOpJournalResponse, MerchantAccountOpJournalResponse.class);
                merchantAccountOpJournalResponse.setTradeType(TradeType.getCommentModuleNo(merchantAccountOpJournalResponse.getAccountType()).getDesc());
                merchantAccountOpJournalResponses.add(merchantAccountOpJournalResponse);
            });
        }
        return new TableResultResponse<>(result.getTotal(), merchantAccountOpJournalResponses);
    }
}