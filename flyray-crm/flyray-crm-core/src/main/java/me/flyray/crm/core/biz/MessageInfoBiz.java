package me.flyray.crm.core.biz;

import me.flyray.crm.core.entity.MessageInfo;
import me.flyray.crm.core.mapper.MessageInfoMapper;
import org.springframework.stereotype.Service;

import me.flyray.common.biz.BaseBiz;

/**
 * 消息表
 *
 * @author centerroot
 * @email ${email}
 * @date 2018-08-21 17:35:49
 */
@Service
public class MessageInfoBiz extends BaseBiz<MessageInfoMapper, MessageInfo> {
}