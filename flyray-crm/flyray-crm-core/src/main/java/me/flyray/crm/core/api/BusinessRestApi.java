package me.flyray.crm.core.api;

import java.util.Map;

import javax.validation.Valid;

import me.flyray.common.msg.BaseApiResponse;
import me.flyray.crm.core.entity.FreezeJournal;
import me.flyray.crm.core.entity.PayChannelConfig;
import me.flyray.crm.core.entity.UnfreezeJournal;
import me.flyray.crm.facade.request.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import me.flyray.crm.core.biz.customer.CustomerAccountQueryBiz;
import me.flyray.crm.core.biz.customer.CustomerAccountTransferBiz;
import me.flyray.crm.core.biz.customer.CustomerFreezeOrUnfreezeBiz;
import me.flyray.crm.core.biz.customer.CustomerIntoAccountBiz;
import me.flyray.crm.core.biz.customer.CustomerOutAccountBiz;
import me.flyray.crm.core.biz.PayChannelConfigBiz;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags="账户管理")
@Controller
@RequestMapping("businessApi")
public class BusinessRestApi{
	private final static Logger logger = LoggerFactory.getLogger(BusinessRestApi.class);
	
	@Autowired
	private PayChannelConfigBiz payChannelConfigBiz;
	@Autowired
	private CustomerIntoAccountBiz customerIntoAccountBiz;
	@Autowired
	private CustomerOutAccountBiz outAccountBiz;
	@Autowired
	private CustomerAccountTransferBiz customerAccountTransferBiz;
	@Autowired
	private CustomerFreezeOrUnfreezeBiz commonFreezeUnFreezeBiz;
	@Autowired
	private CustomerAccountQueryBiz commonAccountQueryBiz;
	
	/**
	 * 手续费计算
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping(value = "/feeManage/calculate", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryMerChannelFee(@RequestBody @Valid MerChannelFeeRequest merChannelFeeRequest){
		String platformId = merChannelFeeRequest.getPlatformId();
		String merchantId = merChannelFeeRequest.getMerchantId();
		String payChannelNo = merChannelFeeRequest.getPayChannelNo();
		String payAmt = merChannelFeeRequest.getPayAmt();
		PayChannelConfig payChannelConfig = new PayChannelConfig();
		payChannelConfig.setPlatformId(platformId);
		payChannelConfig.setMerchantId(merchantId);
		payChannelConfig.setPayChannelNo(payChannelNo);
		Map<String, Object> channelConfig = payChannelConfigBiz.getPayChannelFee(payChannelConfig, payAmt);
		return channelConfig;
	}
	
	/**
	 * 余额充值
	 * 入账（支付）
	 * @return
	 * @throws Exception 
	 */
	@ApiOperation("个人或者商户入账相关的接口")
	@RequestMapping(value = "/accounting/balanceRecharge", method = RequestMethod.POST)
	@ResponseBody
    public BaseApiResponse intoAccount(@RequestBody @Valid IntoAccountRequest intoAccountRequest){
		customerIntoAccountBiz.intoAccount(intoAccountRequest);
		return BaseApiResponse.newSuccess();
    }
	
	/**
	 * 余额支付
	 * @return
	 * @throws Exception 
	 */
	@ApiOperation("个人或者商户转账相关的接口")
	@RequestMapping(value = "/accounting/balancePay", method = RequestMethod.POST)
	@ResponseBody
    public Map<String, Object> accountTransfer(@RequestBody @Valid AccountTransferRequest accountTransferRequest){
		Map<String, Object> response = customerAccountTransferBiz.accountTransfer(accountTransferRequest);
		return response;
    }
	
	/**
	 * 出账（退款）
	 * @return
	 * @throws Exception 
	 */
	@ApiOperation("个人或者商户直接出账的接口")
	@RequestMapping(value = "/accounting/outAccount", method = RequestMethod.POST)
	@ResponseBody
    public BaseApiResponse outAccount(@RequestBody @Valid OutAccountRequest outAccountRequest){
		outAccountBiz.outAccount(outAccountRequest);
		return BaseApiResponse.newSuccess();
    }
	
	/**
	 * 冻结（退款申请）
	 * 余额冻结（提现申请）
	 * @return
	 * @throws Exception 
	 */
	@ApiOperation("冻结接口")
	@RequestMapping(value = "/accounting/freeze", method = RequestMethod.POST)
	@ResponseBody
    public BaseApiResponse<FreezeJournal> freeze(@RequestBody @Valid FreezeRequest freezeRequest){
		FreezeJournal response = commonFreezeUnFreezeBiz.freeze(freezeRequest);
		return BaseApiResponse.newSuccess(response);
	}
	
	/**
	 * ======解冻返还账户===================
	 * 解冻（退款审核失败）
	 * 余额解冻（提现审核失败）
	 * @return
	 * @throws Exception 
	 */
	@ApiOperation("解冻接口")
	@RequestMapping(value = "/accounting/unFreeze", method = RequestMethod.POST)
	@ResponseBody
    public BaseApiResponse<UnfreezeJournal> unfreeze(@RequestBody @Valid UnfreezeRequest unFreezeRequest){
		UnfreezeJournal response = commonFreezeUnFreezeBiz.unfreeze(unFreezeRequest);
		return BaseApiResponse.newSuccess();
    }

	/**
	 * ======解冻扣除金额===================
	 * 解冻并出账（退款审核）
	 * 余额解冻并支付（提现审核）
	 * @return
	 * @throws Exception 
	 */
	@ApiOperation("个人或者商户解冻并出账的接口")
	@RequestMapping(value = "/accounting/unFreAndOutAccount", method = RequestMethod.POST)
	@ResponseBody
    public BaseApiResponse unFreAndOutAccount(@RequestBody @Valid UnfreezeAndOutAccountRequest unFreAndOutAccountRequest){
		outAccountBiz.unfreezeAndOutAccount(unFreAndOutAccountRequest);
		return BaseApiResponse.newSuccess();
	}
	
	/**
	 * ======解冻并出账===================
	 * 解冻并出账至余额账户（退款审核）
	 * @return
	 * @throws Exception 
	 */
	@ApiOperation("解冻并出账至余额账户")
	@RequestMapping(value = "/accounting/unFreAndOutAccToBalance", method = RequestMethod.POST)
	@ResponseBody
	public BaseApiResponse unFreAndOutAccToBalance(@RequestBody @Valid UnfreezeAndTransferRequest unFreezeAndTransferRequest){
		customerAccountTransferBiz.unfreezeAndTransfer(unFreezeAndTransferRequest);
		return BaseApiResponse.newSuccess();
    }
	
	
	/**
	 * 订单状态查询
	 * @return
	 * @throws Exception 
	 */
	@ApiOperation("订单状态查询")
	@RequestMapping(value = "/accounting/queryOrderInfo", method = RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> queryOrderInfo(@RequestBody @Valid AccountingOrderInfoReq accountingOrderInfoReq){
		Map<String, Object> response = commonAccountQueryBiz.queryOrderInfo(accountingOrderInfoReq);
		logger.info("订单状态查询返回:{}",response);
		return response;
    }

}
