package me.flyray.crm.core.modules.personal;

import java.util.Map;

import javax.annotation.Resource;
import javax.validation.Valid;

import me.flyray.common.context.BaseContextHandler;
import me.flyray.common.enums.CustomerType;
import me.flyray.common.msg.BaseApiResponse;
import me.flyray.common.msg.ResponseCode;
import me.flyray.common.vo.QueryPersonalBaseListRequest;
import me.flyray.crm.core.biz.customer.CustomerBaseBiz;
import me.flyray.crm.core.entity.CustomerBase;
import me.flyray.crm.core.entity.PersonalBase;
import me.flyray.crm.facade.request.PersonalBaseRequest;
import me.flyray.crm.facade.request.RegisterRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.personal.PersonalBaseBiz;

/**
 * 个人基础信息
 * @author centerroot
 * @time 创建时间:2018年7月16日下午6:01:37
 * @description
 */
@RestController
@RequestMapping("personalBase")
public class PersonalBaseController extends BaseController<PersonalBaseBiz, PersonalBase> {
	
	@Autowired
	private PersonalBaseBiz personalBaseBiz;
	@Resource
	private CustomerBaseBiz customerBaseBiz;

	
	/**
	 * 查询个人基础信息列表
	 * @author centerroot
	 * @time 创建时间:2018年7月16日下午6:02:04
	 * @param queryPersonalBaseListRequest
	 * @return
	 */
	@RequestMapping(value = "/queryList", method = RequestMethod.POST)
    @ResponseBody
	public Map<String, Object> queryList(@RequestBody @Valid QueryPersonalBaseListRequest queryPersonalBaseListRequest){
		queryPersonalBaseListRequest.setPlatformId(setPlatformId(queryPersonalBaseListRequest.getPlatformId()));
		return personalBaseBiz.queryList(queryPersonalBaseListRequest);
	}
	
	/**
	 * 添加个人基础信息
	 * @time 创建时间:2018年7月16日下午6:02:15
	 * @param personalBaseRequest
	 * @return
	 */
	@RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
	public BaseApiResponse add(@RequestBody @Valid PersonalBaseRequest personalBaseRequest){
		if(StringUtils.isNotBlank(BaseContextHandler.getPlatformId())){
			personalBaseRequest.setPlatformId(BaseContextHandler.getPlatformId());
		}
		if(null == personalBaseRequest.getIsOwner() || 1 != personalBaseRequest.getIsOwner()){
			if("！1".equals(BaseContextHandler.getUserType())){
				personalBaseRequest.setOwnerId(BaseContextHandler.getXId());
				personalBaseRequest.setOwnerName(BaseContextHandler.getXName());
			}
		}
		RegisterRequest registerRequest = new RegisterRequest();
		BeanUtils.copyProperties(personalBaseRequest,registerRequest);
		registerRequest.setCustomerType(CustomerType.CUST_PERSONAL.getCode());
		registerRequest.setPassword("123456");
		if(StringUtils.isBlank(registerRequest.getPhone())){
            return BaseApiResponse.newFailure(ResponseCode.MOBILE_MATCH_FAIL);
        }
        PersonalBase personalBase = personalBaseBiz.queryInfoByMobile(registerRequest.getPhone());
		if(personalBase != null){
            return BaseApiResponse.newFailure(ResponseCode.MOBILE_REPEAT);
        }
		CustomerBase customerBase = customerBaseBiz.register(registerRequest);
		return BaseApiResponse.newSuccess(customerBase);
//		return personalBaseBiz.add(personalBaseRequest);
	}
	
	/**
	 * 查询单个个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:35:45
	 * @param personalId
	 * @return
	 */
	@RequestMapping(value = "/queryOne/{personalId}",method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getOneObj(@PathVariable String personalId){
		Map<String, Object> respMap = baseBiz.getOneObj(personalId);
        return respMap;
    }
	
	/**
	 * 删除个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:35:59
	 * @param personalId
	 * @return
	 */
	@RequestMapping(value = "/deleteObj/{personalId}",method = RequestMethod.DELETE)
    @ResponseBody
    public Map<String, Object> removeObj(@PathVariable String personalId){
		Map<String, Object> respMap = baseBiz.removeObj(personalId);
		return respMap;
    }
	
	/**
	 * 更新个人客户信息
	 * @author centerroot
	 * @time 创建时间:2018年8月13日下午5:36:15
	 * @param personalBaseRequest
	 * @return
	 */
	@RequestMapping(value = "/update",method = RequestMethod.POST)
    @ResponseBody
    public BaseApiResponse updateObj(@RequestBody @Valid PersonalBaseRequest personalBaseRequest){
		if(StringUtils.isNotBlank(BaseContextHandler.getPlatformId())){
			personalBaseRequest.setPlatformId(BaseContextHandler.getPlatformId());
		}
		Boolean result = baseBiz.updateObj(personalBaseRequest);
		if(result){
			return BaseApiResponse.newSuccess();
		}
        return BaseApiResponse.newFailure(ResponseCode.GJJ_UPDATE_ERROR);
    }

	@RequestMapping(value = "/queryPeopleNetwork",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> queryPeopleNetwork(@RequestBody @Valid PersonalBaseRequest personalBaseRequest){
		Map<String, Object> respMap = baseBiz.queryPeopleNetwork(personalBaseRequest);
        return respMap;
    }


}