package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "支付助手app地址配置请求参数")
public class PayHelperUrlConfigrationParam implements Serializable {
	
	@ApiModelProperty(value = "序号")
    private String id;
	
	@ApiModelProperty(value = "平台编号")
	private String platformId;

    @ApiModelProperty(value = "平台名称")
    private String platformName;

	@ApiModelProperty(value = "商户号")
	private String merchantId;
	
	@ApiModelProperty(value = "账号类型 ：支付宝 alipay 微信 wechat")
    private String numberType;
	
	@ApiModelProperty(value = "账号")
    private String number;
	
    @ApiModelProperty(value = "请求地址")
    private String requestUrl;
	
    @ApiModelProperty(value = "状态 00正常 01异常")
    private String txStatus;

    @ApiModelProperty(value = "账户二维码")
    private String accountQrStr;

    @ApiModelProperty(value = "账号昵称")
    private String nickname;

    @ApiModelProperty(value = "姓名")
    private String realName;
}
