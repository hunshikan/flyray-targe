package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 分享记录查询
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "微信小程序分享记录查询请求参数")
public class DistributionRelationQueryRequest {
	
	@NotNull(message="平台编号不能为空")
	@ApiModelProperty(value = "平台编号")
	private String platformId;
	
	@NotNull(message="用户编号不能为空")
	@ApiModelProperty(value = "用户编号")
    private String customerId;
	
	@ApiModelProperty(value = "page")
    private Integer page;
	
	@ApiModelProperty(value = "limit")
    private Integer limit;

}
