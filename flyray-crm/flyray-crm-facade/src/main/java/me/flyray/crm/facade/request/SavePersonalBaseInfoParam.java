package me.flyray.crm.facade.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("录入用户基本信息参数")
public class SavePersonalBaseInfoParam implements Serializable {
	@NotNull(message="用户信息编号不能为空")
	@ApiModelProperty("用户信息编号")
	private String customerId;
	@NotNull(message="信息类型不能为空")
	@ApiModelProperty("信息类型")
	private String infoType;
	@ApiModelProperty("昵称")
	private String nickname;
	@ApiModelProperty("真实姓名")
	private String realName;
	@ApiModelProperty("性别")
	private String sex;
	@ApiModelProperty("生日")
	private String birthday;
	@ApiModelProperty("居住地")
	private String address;
	@ApiModelProperty("家乡")	
	private String hometown;
	@ApiModelProperty("会员ID")	
	private String perId;
	
}
