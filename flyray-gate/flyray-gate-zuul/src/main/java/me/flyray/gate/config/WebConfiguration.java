package me.flyray.gate.config;

import me.flyray.auth.client.interceptor.ServiceAuthRestInterceptor;
import me.flyray.auth.client.interceptor.UserAuthRestInterceptor;
import me.flyray.common.exception.GlobalExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author ace
 * @date 2017/9/8
 */
@Configuration("gateWebConfig")
@Primary
public class WebConfiguration implements WebMvcConfigurer {
    @Bean
    GlobalExceptionHandler getGlobalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
    	registry.addInterceptor(getUserAuthRestInterceptor()).addPathPatterns("/**").addPathPatterns("/admin").excludePathPatterns(getExcludeCommonPathPatterns().toArray(new String[]{}));
    	/*ArrayList<String> commonPathPatterns = getExcludeCommonPathPatterns();
        registry.addInterceptor(getServiceAuthRestInterceptor()).
                addPathPatterns("/**").excludePathPatterns(commonPathPatterns.toArray(new String[]{}));
        registry.addInterceptor(getUserAuthRestInterceptor()).
        		addPathPatterns("/**").excludePathPatterns(commonPathPatterns.toArray(new String[]{}));*/
    }

    @Bean
    ServiceAuthRestInterceptor getServiceAuthRestInterceptor() {
        return new ServiceAuthRestInterceptor();
    }

    @Bean

    UserAuthRestInterceptor getUserAuthRestInterceptor() {
        return new UserAuthRestInterceptor();
    }
    
    private ArrayList<String> getExcludeCommonPathPatterns() {
        ArrayList<String> list = new ArrayList<>();
        String[] urls = {
                "/auth/authorization/token",
                "/auth/authorization/identifyingCode",
                "/ssp/publisher/register",
                "/api/log/save",
                "/swagger-ui.html",
                "/swagger-ui.html/**",
                "/webjars/**"
        };
        Collections.addAll(list, urls);
        return list;
    }

}
