package me.flyray.common.exception.auth;

import me.flyray.common.constant.CommonConstants;
import me.flyray.common.exception.BaseException;

/** 
* @author: bolei
* @date：2018年5月7日 上午8:28:11 
* @description：用户账号异常
*/

public class UserInvalidException extends BaseException {
    public UserInvalidException(String message) {
        super(message, CommonConstants.EX_USER_PASS_INVALID_CODE);
    }
}
