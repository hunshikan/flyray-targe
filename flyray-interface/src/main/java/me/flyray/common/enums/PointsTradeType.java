package me.flyray.common.enums;

/** 
* 交易类型（消费:01，退款:02，提现:03，充值:04，转账:05）
*/

public enum PointsTradeType {

    POINTS_BIZ("06","业务奖励积分"),
    POINTS_RECOMMEND("07","推荐奖励积分"),
    POINTS_DEDUCTION("08","积分冲抵"),
    POINTS_EXCHANGE("09","积分兑换"),
    EXPIRE_POINTS_EXCHANGE("10","过期冲抵")
	;

    private String code;
    private String desc;

    private PointsTradeType(String code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
    
    public static PointsTradeType getCommentModuleNo(String code){
        for(PointsTradeType o : PointsTradeType.values()){
            if(o.getCode().equals(code)){
                return o;
            }
        }
        return null;
    }
}
