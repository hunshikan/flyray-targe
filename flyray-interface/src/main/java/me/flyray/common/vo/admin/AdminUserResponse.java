package me.flyray.common.vo.admin;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author: bolei
 * @date: 17:16 2019/3/8
 * @Description: 类描述
 */
@Data
public class AdminUserResponse implements Serializable {

    private String userId;

    private String username;

    private String password;

    /**
     * 所属部门机构ID
     */
    private Integer deptId;

    /**
     * 所属部门机构名称
     */
    private String deptName;

    private String birthday;

    private String address;

    private String mobilePhone;

    private String telPhone;

    private String email;

    private String sex;

    private String status;

    private String description;

    private Date crtTime;

    private String crtUser;

    private String crtName;

    private String crtHost;

    private Date updTime;

    private String updUser;

    private String updName;

    private String updHost;

    /**
     * 平台编号
     */
    private String platformId;

    /**
     * 商户编号
     */
    private String merchantId;

    /**
     * 用户权限1，系统管理员2，平台管理员3，商户管理员4，平台操作员
     */
    @NotNull(message="用户类型不能为空")
    private Integer userType;

    /**
     * 用户业务角色类型
     */
    private Integer userBizRole;

    /**
     * 省
     */
    private String areaCode;

    /**
     * 市
     */
    private String areaName;

    /**
     * 县
     */
    private String areaLayer;

}
