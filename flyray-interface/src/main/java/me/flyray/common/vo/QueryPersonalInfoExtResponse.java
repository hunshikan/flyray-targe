package me.flyray.common.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;


/**
 * 个人客户信息
 * @time 创建时间:2018年8月14日下午2:47:21
 * @description
 */

@Data
@JsonInclude(JsonInclude.Include.ALWAYS)
public class QueryPersonalInfoExtResponse implements Serializable {
	
	//序号
    private Integer id;
    
	    //个人客户编号
    private String personalId;
	
	    //手机号
	private String phone;
	
	    //用户名称
    private String realName;
	
	    //性别 1：男 2：女
    private String sex;

	    //认证状态  00：未认证  02：认证成功
    private String authenticationStatus;
	
	    //账户状态 00：正常，01：冻结
	//1:未激活 2:已激活 3:客户信息为补全
    private String status;
	    //更新时间
    private String updateTime;
	// 归属人id（存后台管理系统登录人员id）指所属客户经理id
	private String ownerId;

	// 归属人名称（存后台管理系统登录人员名称）所属哪个客户经理
	private String ownerName;
	//客户等级
    private String personalLevel;
	//积分余额
	private String pointsBalance;
	//推荐人Id
	private String referrerId;
	//推荐人名称
	private String referrerName;

}
