package me.flyray.common.msg.auth;

import me.flyray.common.constant.CommonConstants;
import me.flyray.common.msg.BaseApiResponse;

/**
 * Created by ace on 2017/8/23.
 */
public class TokenErrorResponse extends BaseApiResponse {

    public TokenErrorResponse(String message) {
        super(String.valueOf(CommonConstants.EX_CLIENT_INVALID_CODE), message);
    }
}
